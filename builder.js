const delay = ms => new Promise(res => setTimeout(res, ms));

let faculty = document.querySelector("#faculty")
let kurs = document.querySelector("#kurs")
let group = document.querySelector("#group")


let result = {}

const fetch = async () => {

  result["order"] = {};
  result["faculties"] = {};
  result["groups"] = {};

  for (const fac of faculty.children) {
    if (fac.value === 0) continue;

    faculty.value = fac.value

    // noinspection JSUnresolvedFunction
    change_fac(1);

    console.log("Changing fac to " + fac.value)
    result["faculties"][fac.value] = fac.text;

    let facDict = {}
    await delay(1000)

    for (const kr of kurs.children) {
      if (kr.value === "") continue;

      kurs.value = kr.value

      // noinspection JSUnresolvedFunction
      change_fac(0);

      console.log("Changing kurs to " + kr.value)

      let krList = []
      await delay(1000)

      for (const gr of group.children) {
        if (gr.value === "") continue;
        krList.push(gr.value);
        result["groups"][gr.value] = gr.text;
      }

      if(krList.length > 0) facDict[kr.value] = krList;
    }

    result["order"][fac.value] = facDict;

  }

  // Cleanup data a bit
  delete result["order"]["0"];
  delete result["faculties"]["0"];
  delete result["groups"]["0"];
}

fetch().then(() => console.log(JSON.stringify(result)))
