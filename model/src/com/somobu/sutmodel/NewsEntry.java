package com.somobu.sutmodel;

public class NewsEntry {

    /**
     * Millis since unix timestamp
     */
    public long time;

    /**
     * ~40 first symbols will be shown in preview
     */
    public String title;

    /**
     * ~40 first symbols will be shown in preview
     */
    public String message;

    /**
     * User can hide/dismiss this entry
     */
    public boolean dismissible = true;
    
    public UrlBtn url;
    public Filter filter;

    public static class UrlBtn {

        /**
         * About 8 characters long
         */
        public String label;

        public String url;
        
    }

    public static class Filter {

        /**
         * App version code, inclusive
         */
        public int sinceVer;

        /**
         * App version code, inclusive
         */
        public int untilVer;

        /**
         * Millis since unix timestamp
         */
        public long sinceDate;

        /**
         * Millis since unix timestamp
         */
        public long untilDate;

        /**
         * Should be shown only in debug version
         */
        public boolean debugOnly = false;
    }

}
