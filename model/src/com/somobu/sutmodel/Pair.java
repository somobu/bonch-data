package com.somobu.sutmodel;

public class Pair {

    public enum Type {
        LECT,
        PRACT,
        LABA,
        
        ZACH,
        CONS,
        EXAM,
        
        UNKNOWN
    }

    /**
     * Use `starts` instead
     */
    @Deprecated
    public int[] weeks;

    /**
     * Pair start in milliseconds.
     */
    public long[] starts;
    
    public int weekday;
    public int pair;

    public int subject;     // Nullable
    public Type type;
    public int[] teachers;  // May be 0-length
    public int[] groups;    // May be 0-length
    public String aud;      // Nullable

}
