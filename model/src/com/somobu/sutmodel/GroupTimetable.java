package com.somobu.sutmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupTimetable {

    public Map<Integer, String> teachers = new HashMap<>();
    public Map<Integer, String> subjects = new HashMap<>();
    public List<Pair> pairs = new ArrayList<>();

}
