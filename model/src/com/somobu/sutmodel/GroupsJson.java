package com.somobu.sutmodel;

import java.util.List;
import java.util.Map;

public class GroupsJson {

    public Map<String, String> faculties;
    public Map<String, String> groups;

    /**
     * Map: Faculty - course - group number
     */
    public Map<String, Map<String, List<String>>> order;
}
