package com.somobu.sutparser;

import com.somobu.sutmodel.Pair;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;

/**
 * Loads data from `cabinet.sut.ru/raspisanie_all_new` which is marked as "unsupported"
 */
public class ParserLegacy {

    private static final boolean DEBUG_CONNECT = false;
    private static final boolean DEBUG_PARSE = false;

    /**
     * По какой-то причине начиная со второго семестра 18/19 учебного года
     * айдишники номеров регулярных пар (1-5) смещены на единицу вниз (т.е. 2-6).
     * <br></br><br></br>
     * Это поле - костыль на случай, если регулярные пары опять куда-то
     * сместятся.
     * <br></br><br></br>
     * Примечательно, что на физкультуры (83-87) это правило не распространяется
     */
    private static final int regularPairOffset = 1;


    static final String fmt = "https://cabinet.sut.ru/raspisanie_all_new"
            + "?schet=%s" + "&type_z=1&group_el=0" + "&group=%s";

    static final String schet = "205.2324/2"; // Семестр (весна 2024)
    static final long currentSemesterStart = 1707685200000L; // 2024-02-12


    static class RawPair {
        int[] weeks;
        int weekday;
        int pair;

        String subject;     // Nullable
        Pair.Type type;
        String[] teachers;  // May be 0-length, may contain empty string
        String aud;         // Nullable

        @Override
        public String toString() {
            return weekday + "/" + pair + " - " + subject + " " + type + " - " + Arrays.toString(teachers)
                    + " " + aud + " " + Arrays.toString(weeks);
        }
    }

    public static List<RawPair> parse(String group) throws IOException, NumberFormatException {
        String targetUrl = String.format(fmt, schet, group);

        ArrayList<RawPair> parsedPairs = new ArrayList<>();

        if (DEBUG_CONNECT) v("Started...");

        if (targetUrl == null) throw new RuntimeException("You have to provide `targetUrl`!");

        if (DEBUG_CONNECT) System.out.println("RQ " + targetUrl);

        Connection conn = Jsoup.connect(targetUrl)
                .header("Referer", "https://cabinet.sut.ru/raspisanie_all_new")
                .userAgent("Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0")
                .timeout(5 * 60 * 1000)
                .method(Connection.Method.POST);

        String key = targetUrl.substring(targetUrl.indexOf("schet=") + "schet=".length());
        int idx = key.indexOf("&");
        key = key.substring(0, idx > 0 ? idx : key.length());
        key = key.replace("%2F", "/");

        if (DEBUG_CONNECT) System.out.println("Got key " + key);

        Connection.Response response = conn.execute();
        if (DEBUG_CONNECT) v("Connected...");

        Document document = response.parse();

        Elements pairs = document.getElementsByClass("pair");

        // Parse result
        for (Element el : pairs) {
            if (DEBUG_PARSE) v(el.html());

            RawPair pair = new RawPair();

            pair.weekday = Integer.parseInt(el.attr("weekday"));
            pair.pair = Integer.parseInt(el.attr("pair"));

            String rawType = el.getElementsByClass("type").get(0).text();
            pair.type = parseType(rawType);

            String rawWeeks = el.getElementsByClass("weeks").get(0).text();
            pair.weeks = parseDirtyWeekNumbers(rawWeeks);

            String subject = el.getElementsByClass("subect").get(0).text();
            if (subject.trim().isEmpty()) subject = null;
            pair.subject = subject;

            Elements audEls = el.getElementsByClass("aud");
            if (audEls.size() > 0) {
                pair.aud = audEls.get(0).text().substring(6);
            } else {
                pair.aud = null;
            }

            Elements teacherEl = el.getElementsByClass("teacher");
            if (teacherEl.size() > 0) {
                String teacher;
                if (teacherEl.get(0).hasAttr("title")) {
                    teacher = teacherEl.get(0).attr("title");
                } else {
                    teacher = teacherEl.get(0).text();
                }

                HashSet<String> teachers = new HashSet<>();
                for (String t : teacher.split(";")) {
                    String trimmed = t.trim();
                    if (!trimmed.isEmpty()) {
                        if ("- - -".equals(trimmed)) {
                            teachers.add("");
                        } else {
                            teachers.add(trimmed);
                        }
                    }
                }

                pair.teachers = teachers.toArray(new String[0]);
            } else {
                pair.teachers = new String[0];
            }

            parsedPairs.add(pair);
        }

        return parsedPairs;
    }

    private static int[] parseDirtyWeekNumbers(String data) {
        String clearData = data.replaceAll("[^\\d\\s]", "");
        String[] stringArray = clearData.split(" ");
        int[] output = new int[stringArray.length];

        for (int i = 0; i < output.length; i++) {
            if (stringArray[i].trim().isEmpty()) {
                output[i] = -11;
            }

            try {
                output[i] = Integer.parseInt(stringArray[i]);
            } catch (Exception e) {
                throw new RuntimeException("Error while parsing dirty array \"" + clearData + "\":\n\t", e);
            }
        }

        return output;
    }

    private static Pair.Type parseType(String type) {
        switch (type.trim().toLowerCase()) {
            case "(лекция)":
                return Pair.Type.LECT;
            case "(практические занятия)":
                return Pair.Type.PRACT;
            case "(лабораторная работа)":
                return Pair.Type.LABA;
            default:
                return Pair.Type.UNKNOWN;
        }
    }

    private static void v(String s) {
        System.out.println(s);
    }


    /**
     * Костыль. Убрать при рефакторе
     */
    @Deprecated
    public static int getBonchWeekOf(Date date) {
        Calendar start = new GregorianCalendar();
        start.setTime(new Date(currentSemesterStart));

        Calendar end = new GregorianCalendar();
        end.setTime(date);

        return end.get(Calendar.WEEK_OF_YEAR) - start.get(Calendar.WEEK_OF_YEAR) + 1;
    }

    public static long[] getStartsFromBonchWeeks(int weekday, int pair, int[] weeks) {
        long[] rz = new long[weeks.length];
        for (int i = 0; i < weeks.length; i++) {
            rz[i] = asMillis(weeks[i], weekday, pair);
        }

        return rz;
    }

    private static long asMillis(int week, int weekday, int pair) {
        long result = currentSemesterStart;
        long millisInDay = (1000L * 60 * 60 * 24);
        long millisInWeek = millisInDay * 7L;
        result += (week - 1) * millisInWeek;
        result += (weekday - 1) * millisInDay;
        result += pairInMillisOffset(pair);
        return result;
    }

    @SuppressWarnings("DuplicateBranchesInSwitch")
    public static long pairInMillisOffset(int pair) {
        long rz = 0;

        switch (pair) {
            case 1 + regularPairOffset:
                rz = timeOffset(9, 0);
                break;
            case 2 + regularPairOffset:
                rz = timeOffset(10, 45);
                break;
            case 3 + regularPairOffset:
                rz = timeOffset(13, 0);
                break;
            case 4 + regularPairOffset:
                rz = timeOffset(14, 45);
                break;
            case 5 + regularPairOffset:
                rz = timeOffset(16, 30);
                break;
            case 6 + regularPairOffset:
                rz = timeOffset(18, 15);
                break;
            case 30:
                rz = timeOffset(20, 0);
                break;
            case 69:
                rz = timeOffset(9, 50);
                break;
            case 83:
                rz = timeOffset(9, 0); // Обычно это физкультуры
                break;
            case 84:
                rz = timeOffset(10, 30);
                break;
            case 85:
                rz = timeOffset(12, 0);
                break;
            case 86:
                rz = timeOffset(13, 30);
                break;
            case 87:
                rz = timeOffset(15, 0);
                break;
            default:
                System.out.println("No offset for " + pair);
        }

        return rz;
    }

    private static long timeOffset(int hour, int minutes) {
        return hour * 60 * 60 * 1000L + minutes * 60 * 1000L;
    }

}
