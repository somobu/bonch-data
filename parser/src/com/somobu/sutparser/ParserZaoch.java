package com.somobu.sutparser;

import com.somobu.sutmodel.GroupsJson;
import com.somobu.sutmodel.Pair;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Loads data of "Заочники" from new (as for 2022) source - `www.sut.ru/studentu/raspisanie`
 */
public class ParserZaoch {

    private static final boolean DEBUG_CONNECT = false;
    private static final boolean DEBUG_PARSE = false;

    public static String INO = "ino";
    public static String K = "1";

    static class RawPair {

        int weekday;
        int pair;

        Date day;
        Pair.Type type;
        String subject;
        String[] teachers;
        String aud;

        @Override
        public String toString() {
            return "RawPair{" +
                    "weekday=" + weekday +
                    ", pair=" + pair +
                    ", day=" + day +
                    ", type=" + type +
                    ", subject='" + subject + '\'' +
                    ", teachers='" + Arrays.toString(teachers) + '\'' +
                    ", aud='" + aud + '\'' +
                    '}';
        }
    }

    private static final String groupsUrl = "https://www.sut.ru/studentu/raspisanie/raspisanie-sessii-studentov-zaochnoy-formi-obucheniya";
    private static final String baseUrl = "https://www.sut.ru/studentu/raspisanie/raspisanie-sessii-studentov-zaochnoy-formi-obucheniya?group=%s";
    private static final String userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/116.0";
    
    private static final SimpleDateFormat parser = new SimpleDateFormat("dd.MM.yyyy");

    /**
     * Adds own faculty (@see INO) and groups
     */
    public static void patchGroups() throws IOException {
        GroupsJson json = FileIO.readGroups();
        json.faculties.put(INO, "ИНО (Заочная форма обучения)");
        json.order.put(INO, new HashMap<>());
        json.order.get(INO).put(K, new ArrayList<>());

        Connection conn = Jsoup.connect(groupsUrl)
                .header("Referer", "https://cabinet.sut.ru/raspisanie_all_new")
                .userAgent(userAgent)
                .timeout(5 * 60 * 1000)
                .method(Connection.Method.GET);

        Connection.Response response = conn.execute();
        if (DEBUG_CONNECT) v("Connected...");

        Document document = response.parse();

        Elements groups = document.getElementsByClass("vt256");
        for (Element g : groups) {
            if (DEBUG_PARSE) v(g.html());

            String groupId = g.attr("data-i");
            String groupName = g.attr("data-nm");

            json.groups.put(groupId, groupName);
            json.order.get(INO).get(K).add(groupId);
        }

        FileIO.write(json);
    }

    public static List<RawPair> parse(String groupId) throws IOException, ParseException {
        ArrayList<RawPair> result = new ArrayList<>();

        if (DEBUG_CONNECT) v("Started...");

        String url = String.format(baseUrl, groupId);
        if (DEBUG_CONNECT) System.out.println("RQ " + url);

        Connection conn = Jsoup.connect(url)
                .header("Referer", "https://cabinet.sut.ru/raspisanie_all_new")
                .userAgent("Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0")
                .timeout(5 * 60 * 1000)
                .method(Connection.Method.GET);

        Connection.Response response = conn.execute();
        if (DEBUG_CONNECT) v("Connected...");

        Document document = response.parse();

        Elements pairs = document.getElementsByClass("pair");

        // Parse result
        for (Element el : pairs) {
            if (DEBUG_PARSE) v(el.html());

            RawPair pair = new RawPair();

            pair.weekday = Integer.parseInt(el.attr("weekday"));
            pair.pair = Integer.parseInt(el.attr("pair"));

            Elements tds = el.getElementsByTag("td");

            pair.day = parseDay(tds.get(0).text());
            pair.type = parseType(tds.get(2).text());
            pair.subject = tds.get(3).text();
            pair.teachers = normTeachers(tds.get(4).text()).toArray(new String[0]);
            pair.aud = normAud(tds.get(5).text());

            result.add(pair);
        }

        return result;
    }

    private static Date parseDay(String day) throws ParseException {
        return parser.parse(day.substring(0, 10));
    }

    private static Pair.Type parseType(String type) {
        switch (type.trim().toLowerCase()) {
            case "лекция":
                return Pair.Type.LECT;
            case "практические занятия":
                return Pair.Type.PRACT;
            case "лабораторная работа":
                return Pair.Type.LABA;
            case "зачет":
                return Pair.Type.ZACH;
            case "консультация":
                return Pair.Type.CONS;
            case "экзамен":
                return Pair.Type.EXAM;
            default:
                return Pair.Type.UNKNOWN;
        }
    }

    private static Set<String> normTeachers(String teachers) {
        String[] replacers = {
                " СС и ПД", " ТОТ", " ИКС", " РОС", " ЗСС", " ФиЛС", " КПРЭС", " РСиВ", " СПН", " ПиВТ",
                " БИ", " УМСЭС", " ИУС", " ТВ и М", " ФК", " ЭМИ", " ВМ", "ФИСИС", " Э и С", " ИРВ", " ЭБТ",
                " ИСАУ", " ИЯ", " ФСЦТ", " ИКД", " ИН и РЯ", " АПС", " Физики",
                " ст.пр.", " доцент", ",доцент", " ассистент", " профессор",
                ",к.т.н.", ",с.н.с.", " к.соц.н.", ",к.п.н.", ",д.т.н.", ",к.э.н.", ",д.пед.н.", ",к.псх.н.",
                ",к.пол.н.", ",к.ф.-м.н.", ",к.г.н.", " к.воен.н.", ",к.филол.н.", ",к.филос.н.", ",д.соц.н.",
                ",д.э.н.",
                ",Член-корреспондент отраслевой академии наук",
                ",зав.кафедрой", " Учебная часть преподаватель",
                " преподаватель", " Доцент", ",Доцент", " Профессор", ",профессор"
        };
        String rz = teachers;

        for (String replacer : replacers) rz = rz.replace(replacer, "");

        HashSet<String> teachersSet = new HashSet<>();
        for (String t : rz.split(";")) {
            String trimmed = t.trim();
            if (!trimmed.isEmpty()) {
                if ("- - -".equals(trimmed)) teachersSet.add("");
                else teachersSet.add(trimmed);
            }
        }

        return teachersSet;
    }

    private static String normAud(String aud) {
        return aud.replace("; пр.Большевиков,22,к.", "; Б22/");
    }

    private static void v(String s) {
        System.out.println(s);
    }
}
