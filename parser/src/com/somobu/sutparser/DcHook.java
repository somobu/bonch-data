package com.somobu.sutparser;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * Just to keep all Discord-related stuff in one place
 */
public class DcHook {

    public static void reportError(Exception e) throws IOException {
        String hook = System.getenv("DC_HOOK");
        if (hook != null && !hook.trim().isEmpty()) {
            triggerHook(hook, e);
        } else {
            System.out.println("DcHook: skipping -- no url provided");
        }
    }


    private static class HookData {
        String content;
    }

    private static void triggerHook(String hook, Exception e) throws IOException {

        HookData data = new HookData();
        data.content = String.format("```\n%s\n```", e.getMessage());
        String message = FileIO.gson.toJson(data);

        HttpURLConnection http = (HttpURLConnection) new URL(hook).openConnection();
        http.setRequestMethod("POST");
        http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        http.setRequestProperty("User-Agent", "DiscordBot (somobu-lib, 1.0)");
        http.setDoOutput(true);

        byte[] bytes = message.getBytes(StandardCharsets.UTF_8);
        http.setFixedLengthStreamingMode(bytes.length);
        http.connect();

        OutputStream os = http.getOutputStream();
        os.write(bytes);
        os.flush();
        os.close();

        int rc = http.getResponseCode();
        System.out.println("Hook resulted with " + rc);
        http.disconnect();
    }

}
