package com.somobu.sutparser;

import com.somobu.sutmodel.Pair;

import java.util.ArrayList;

/**
 * Just a few convenience classes for Gson serialization
 */
public class Conv {

    public static class PairList extends ArrayList<Pair> {

    }

    public static class StringList extends ArrayList<String> {

    }

}
