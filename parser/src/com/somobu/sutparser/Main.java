package com.somobu.sutparser;

import com.somobu.sutmodel.GroupsJson;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

public class Main {

    static final long COOLDOWN = (long) (3.2 * 1000);

    /**
     * true: remove old saved data and fetch new one from server
     * false: restore fetched raw data and re-process it
     */
    static final boolean FETCH_NEW = true;

    static final Data data = new Data();

    public static void main(String[] args) throws Exception {
        try {
            if (FETCH_NEW) {
                System.out.println("Removing old data...");
                FileIO.rmOldData();

                System.out.println("Adding zaoch timetable on top of groups.json...");
                ParserZaoch.patchGroups(); // Add zaoch groups

                System.out.println("Fetching new data...");
                fetchTimetables(FileIO.readGroups());
                fetchTimetablesZaoch(FileIO.readGroups());
                System.out.println();

                System.out.println("Saving raws...");
                FileIO.saveFetched();
            } else {
                System.out.println("Restoring raws...");
                FileIO.restoreFetched();
            }

            System.out.println("Processing data...");
            data.recalcCompleteGroups(); // Would take about 15s
            
            System.out.println("Saving data...");
            for (String gid : data.getGroups()) FileIO.write(gid, data.getGroupTimetable(gid));
            for (String teacher : data.getTeachers()) FileIO.write(teacher, data.getTeacherTimetable(teacher));

            System.out.println("Done");
        } catch (Exception e) {
            DcHook.reportError(e);
            throw e;
        }
    }


    static void fetchTimetables(GroupsJson idx) throws IOException, InterruptedException {
        System.out.print("Fetching timetables...");

        int facCount = idx.order.size();
        int currentFac = 1;

        for (String faculty : idx.order.keySet()) {
            if (faculty.equals(ParserZaoch.INO)) continue;

            Map<String, List<String>> facultyObj = idx.order.get(faculty);

            int kursCount = facultyObj.keySet().size();
            int currentKurs = 1;

            for (String kurs : facultyObj.keySet()) {
                List<String> kursArr = facultyObj.get(kurs);

                int groupCount = kursArr.size();
                int currentGroup = 1;

                for (String group : kursArr) {
                    System.out.printf(
                            "\rFac %d/%d > Kurs %d/%d > Group %d/%d     ",
                            currentFac, facCount,
                            currentKurs, kursCount,
                            currentGroup, groupCount
                    );


                    Thread.sleep(COOLDOWN);
                    List<ParserLegacy.RawPair> pairs = ParserLegacy.parse(group);

                    data.accumulatePairs(group, pairs);

                    currentGroup++;
                }

                currentKurs++;
            }

            currentFac++;
        }
    }

    static void fetchTimetablesZaoch(GroupsJson idx) throws InterruptedException, IOException, ParseException {
        int groupCount = idx.order.get(ParserZaoch.INO).get(ParserZaoch.K).size();
        int currentGroup = 1;
        for (String group : idx.order.get(ParserZaoch.INO).get(ParserZaoch.K)) {
            System.out.printf("\rZaoch > Group %d/%d            ", currentGroup, groupCount);

            Thread.sleep(COOLDOWN);
            List<ParserZaoch.RawPair> pairs = ParserZaoch.parse(group);
            data.accumulatePairsZ(group, pairs);

            currentGroup += 1;
        }
    }

}
