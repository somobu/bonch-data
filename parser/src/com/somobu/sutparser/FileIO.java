package com.somobu.sutparser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.somobu.sutmodel.GroupTimetable;
import com.somobu.sutmodel.GroupsJson;
import com.somobu.sutmodel.Pair;
import com.somobu.sutmodel.TeacherTimetable;

import java.io.*;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class FileIO {

    static Gson gson = new GsonBuilder().create();

    public static void rmOldData() {
        File[] foldersToClean = {
                new File("data/raw_pairs"),
                new File("data/by_group"),
                new File("data/by_teacher")
        };

        for (File folder : foldersToClean) {
            if (!folder.exists() && !folder.mkdirs()) throw new RuntimeException();
            for (File f : folder.listFiles()) f.delete();
        }
    }

    public static void restoreFetched() throws FileNotFoundException {
        for (File f : new File("data/raw_pairs").listFiles()) {
            String fname = f.getName();
            String groupNo = fname.substring(0, fname.indexOf("."));

            Conv.PairList list = gson.fromJson(new JsonReader(new FileReader(f)), Conv.PairList.class);
            Main.data.setPairs(groupNo, list);
        }

        Main.data.setSubjects(gson.fromJson(new JsonReader(new FileReader("data/subjects_idx.json")), Conv.StringList.class));
        Main.data.setTeachers(gson.fromJson(new JsonReader(new FileReader("data/teachers_idx.json")), Conv.StringList.class));
    }

    public static GroupsJson readGroups() throws IOException {
        String resourceName = "data/groups.json";
        InputStream is = Files.newInputStream(new File(resourceName).toPath());
        Scanner s = new Scanner(is).useDelimiter("\\A");
        String result = s.hasNext() ? s.next() : "";

        Gson gson = new GsonBuilder().create();
        return gson.fromJson(result, GroupsJson.class);
    }


    static void saveFetched() throws IOException {
        writePairsByGroup(new File("data/raw_pairs"), Main.data.getPairsByGroup());
        write(new File("data/subjects_idx.json"), gson.toJson(Main.data.getSubjectsIndex()));
        write(new File("data/teachers_idx.json"), gson.toJson(Main.data.getTeachersIndex()));
    }

    static void writePairsByGroup(File folder, HashMap<String, List<Pair>> pairsByGroup) throws IOException {
        for (String key : pairsByGroup.keySet()) {
            List<Pair> list = pairsByGroup.get(key);
            write(new File(folder, key + ".json"), gson.toJson(list));
        }
    }

    static void write(GroupsJson g) throws IOException {
        write(new File("data/groups.json"), gson.toJson(g));
    }

    static void write(String groupName, GroupTimetable tt) throws IOException {
        write(new File("data/by_group/" + groupName + ".json"), gson.toJson(tt));
    }

    static void write(String teacherName, TeacherTimetable tt) throws IOException {
        write(new File("data/by_teacher/" + teacherName + ".json"), gson.toJson(tt));
    }

    static void write(File file, String rz) throws IOException {
        try {
            file.delete();
        } catch (Exception ignored) {

        }
        file.createNewFile();


        FileOutputStream outputStream = new FileOutputStream(file);
        byte[] strToBytes = rz.getBytes();
        outputStream.write(strToBytes);
        outputStream.close();
    }

}
