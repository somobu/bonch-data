package com.somobu.sutparser;

import com.somobu.sutmodel.GroupTimetable;
import com.somobu.sutmodel.Pair;
import com.somobu.sutmodel.TeacherTimetable;

import java.util.*;

/**
 * Intended to hold all timetable data
 * <br/>
 * <br/>The idea is, this class is single source of truth for timetables
 * <br/>Also, the only one who does all transformation/modification on this data is this class itself
 */
public class Data {

    
    // DATA ITSELF //

    private ArrayList<String> subjectsIndex = new ArrayList<>();
    private ArrayList<String> teachersIndex = new ArrayList<>();
    private HashMap<String, List<Pair>> pairsByGroup = new HashMap<>();


    // (DE)SERIALIZATION STUFF //

    public void setPairs(String group, List<Pair> pairs) {
        pairsByGroup.put(group, pairs);
    }

    public void setSubjects(ArrayList<String> subjects) {
        subjectsIndex = subjects;
    }

    public void setTeachers(ArrayList<String> teachers) {
        teachersIndex = teachers;
    }

    public ArrayList<String> getSubjectsIndex() {
        return subjectsIndex;
    }

    public ArrayList<String> getTeachersIndex() {
        return teachersIndex;
    }

    public HashMap<String, List<Pair>> getPairsByGroup() {
        return pairsByGroup;
    }


    // ADD+UPDATE DATA //

    public void accumulatePairs(String group, List<ParserLegacy.RawPair> pairs) {
        pairsByGroup.put(group, new ArrayList<>());

        int groupNo = Integer.parseInt(group);
        for (ParserLegacy.RawPair pair : pairs) {
            accumulatePair(pair, groupNo);
        }
    }

    public void accumulatePairsZ(String group, List<ParserZaoch.RawPair> pairs) {
        if (pairsByGroup.containsKey(group)) {
            System.err.println("Group " + group + " was overridden by zaoch timetable");
        }

        pairsByGroup.put(group, new ArrayList<>());

        int groupNo = Integer.parseInt(group);
        for (ParserZaoch.RawPair pair : pairs) {
            accumulatePair(pair, groupNo);
        }
    }


    private void accumulatePair(ParserLegacy.RawPair srcPair, int groupNo) {
        Pair pair = new Pair();
        pair.weeks = srcPair.weeks;
        pair.starts = ParserLegacy.getStartsFromBonchWeeks(srcPair.weekday, srcPair.pair, srcPair.weeks);
        pair.weekday = srcPair.weekday;
        pair.pair = srcPair.pair;

        int subjectIdx = subjectsIndex.indexOf(srcPair.subject);
        if (subjectIdx < 0) {
            subjectIdx = subjectsIndex.size();
            subjectsIndex.add(srcPair.subject);
        }
        pair.subject = subjectIdx;

        pair.type = srcPair.type;

        pair.teachers = new int[srcPair.teachers.length];
        for (int i = 0; i < srcPair.teachers.length; i++) {
            int teacherIdx = teachersIndex.indexOf(srcPair.teachers[i]);
            if (teacherIdx < 0) {
                teacherIdx = teachersIndex.size();
                teachersIndex.add(srcPair.teachers[i]);
            }
            pair.teachers[i] = teacherIdx;
        }

        pair.groups = new int[]{groupNo};
        pair.aud = srcPair.aud;

        pairsByGroup.get("" + groupNo).add(pair);
    }

    private void accumulatePair(ParserZaoch.RawPair srcPair, int groupNo) {
        Pair pair = new Pair();
        pair.weeks = new int[]{ParserLegacy.getBonchWeekOf(srcPair.day)};
        pair.starts = new long[]{srcPair.day.getTime() + ParserLegacy.pairInMillisOffset(srcPair.pair)};
        pair.weekday = srcPair.weekday;
        pair.pair = srcPair.pair;

        int subjectIdx = subjectsIndex.indexOf(srcPair.subject);
        if (subjectIdx < 0) {
            subjectIdx = subjectsIndex.size();
            subjectsIndex.add(srcPair.subject);
        }
        pair.subject = subjectIdx;

        pair.type = srcPair.type;

        pair.teachers = new int[srcPair.teachers.length];
        for (int i = 0; i < srcPair.teachers.length; i++) {
            int teacherIdx = findUnabbreviatedTeacher(srcPair.teachers[i]);
            if (teacherIdx < 0) {
                teacherIdx = teachersIndex.size();
                teachersIndex.add(srcPair.teachers[i]);
            }
            pair.teachers[i] = teacherIdx;
        }

        pair.groups = new int[]{groupNo};
        pair.aud = srcPair.aud;

        pairsByGroup.get("" + groupNo).add(pair);
    }


    // FINAL MODIFICATION/RECALCULATION //

    public void recalcCompleteGroups() {
        for (List<Pair> pairList : pairsByGroup.values()) {
            for (Pair pair : pairList) {
                pair.groups = groupsOf(pair.weeks, pair.weekday, pair.pair, pair.aud);
            }
        }
    }


    // GETTERS //

    public Set<String> getGroups() {
        return pairsByGroup.keySet();
    }

    public Set<String> getTeachers() {
        return new HashSet<>(teachersIndex);
    }

    public GroupTimetable getGroupTimetable(String group) {
        GroupTimetable result = new GroupTimetable();

        result.pairs = pairsByGroup.get(group);
        for (Pair pair : result.pairs) {
            for (int tid : pair.teachers) result.teachers.put(tid, teachersIndex.get(tid));
            result.subjects.put(pair.subject, subjectsIndex.get(pair.subject));
        }

        return result;
    }

    public TeacherTimetable getTeacherTimetable(String teacher) {
        TeacherTimetable tt = new TeacherTimetable();

        List<Pair> allPairs = new ArrayList<>();
        int tid = teachersIndex.indexOf(teacher);
        for (List<Pair> pairList : pairsByGroup.values()) {
            for (Pair pair : pairList) {
                for (int pairTid : pair.teachers) {
                    if (pairTid == tid) allPairs.add(pair);
                }
            }
        }

        for (Pair pair : allPairs) {
            boolean exists = false;
            for (Pair existingPair : tt.pairs) {
                if (pair.pair == existingPair.pair && pair.weekday == existingPair.weekday
                        && Arrays.equals(pair.weeks, existingPair.weeks)) {
                    exists = true;
                    break;
                }
            }

            if (!exists) {
                tt.pairs.add(pair);
                tt.subjects.put(pair.subject, subjectsIndex.get(pair.subject));
            }
        }

        return tt;
    }


    // INNER HELPERS //

    private int findUnabbreviatedTeacher(String abbreviated) {
        String lastName = abbreviated.trim().replaceAll("\\s\\S+$", "");

        for (int i = 0; i < teachersIndex.size(); i++) {
            if (teachersIndex.get(i).startsWith(lastName)) {
                return i;
            }
        }

        return -1;
    }

    private int[] groupsOf(int[] weeks, int weekday, int pairNo, String aud) {
        HashSet<Integer> groups = new HashSet<>();

        for (List<Pair> pairList : pairsByGroup.values()) {
            for (Pair pair : pairList) {
                if (pair.pair == pairNo && pair.weekday == weekday) {
                    boolean weeksEq = Arrays.equals(pair.weeks, weeks);
                    boolean audEq = Objects.equals(pair.aud, aud);

                    if (weeksEq && audEq) {
                        for (int i : pair.groups) groups.add(i);
                    }
                }
            }
        }

        int[] rz = new int[groups.size()];
        int i = 0;
        for (Integer group : groups) {
            rz[i] = group;
            i++;
        }

        return rz;
    }

}
