# Data processing repo of Bonch.Timetable

## Что есть

**model** - либа, описывает какие json'ы рожает `parser`, в готовом виде лежит в [собственной m2 репе][sutparser_m2].

**parser** - консольная утилита. В соответствии с `data/groups.json` стягивает расписания всех групп,
пересобирает их в удобные для [просмотрщика расписания][bonch_timetable] json'ы и кладет их в `data`.

**builder.js** - тулза для того, чтобы выцепить список известных групп. Использование: воткнуть в консоль браузера
на странице `https://cabinet.sut.ru/raspisanie_all_new`, дождаться завершения, итоговый json положить в
`data/groups.json`. Необходима как для работы `parser` (по этому списку он выцепит расписания), так и для [просмотрщика][bonch_timetable] (чтобы показать юзеру список групп).

[sutparser_m2]: https://gitlab.com/somobu/m2

[bonch_timetable]: https://gitlab.com/somobu/bonch-timetable

## Иерархия

```
data/
  by_group/             пары по айдишникам групп (каждый файл - объект GroupTimetable)
  by_teacher/           пары по именам преподов (каждый файл - объект TeacherTimetable)
  raw_pairs/            "сырые" пары по id групп (список объектов Pair)
  groups.json           список групп (GroupsJson)
  subjects_idx.json     полный список названий предметов (позиция в списке - id предмета)
  teachers_ids.json     полный список имен учителей (позиция в списке - id препода)
```

## Как запускать `parser`

### Через gradle

```bash
gradle run
```

N.B.: убедись, что в `parser/data` лежит `groups.json` и он валиден.

### jar'кой

Сначала нужно собрать полную jar'ку через

```bash
gradle fatjar
```

Затем забрать ее из `parser/build/libs` и запускать как обычно

```bash
java -jar parser.jar
```

### Доп.инфа

- Если установлена переменная окружения `DC_HOOK`, то при ошибке `parser` использует ее в качестве
  [Discord Hook URL][dc_hook] по которому запостит сообщение с текстом ошибки;

[dc_hook]: https://discord.com/developers/docs/resources/webhook#execute-webhook

## Как обновлять между семестрами

- Пхаешь `builder.js` в `https://cabinet.sut.ru/raspisanie_all_new`;
- Результат ложишь в `groups.json`;
- Обновляешь в `parser::ParserLegacy` поля `schet` и `currentSemesterStart`;
- Пересобираешь jar'ку (`./gradlew parser:fatjar`), грузишь ее на серв вместе с `groups.json`;


